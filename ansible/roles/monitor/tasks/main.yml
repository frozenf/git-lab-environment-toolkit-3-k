---
- name: Propagate Secrets if existing
  include_role:
    name: common
    tasks_from: secrets
  tags:
    - reconfigure
    - secrets

- name: Import GitLab Grafana dashboards
  include_tasks: dashboard_import.yml
  vars:
    monitor_custom_dashboards_path: "{{ role_path }}"
  loop:
    - { display_name: 'GitLab Environment Toolkit', folder: 'files' }
  tags:
    - reconfigure
    - dashboards

- name: Import custom Grafana dashboards
  include_tasks: dashboard_import.yml
  loop: "{{ monitor_custom_dashboards }}"
  tags:
    - reconfigure
    - dashboards

- name: Setup GitLab config file
  template:
    src: templates/monitor.gitlab.rb.j2
    dest: /etc/gitlab/gitlab.rb
  tags: reconfigure

- name: Check if custom config exists
  stat:
    path: "{{ monitor_custom_config_file }}"
  delegate_to: localhost
  become: false
  tags: reconfigure
  register: monitor_custom_config_file_path

- name: Setup Custom Config
  template:
    src: "{{ monitor_custom_config_file }}"
    dest: "/etc/gitlab/gitlab.monitor.custom.rb"
    mode: 0644
  tags: reconfigure
  when: monitor_custom_config_file_path.stat.exists

- name: Copy over any Custom Files
  copy:
    src: "{{ item.src_path }}"
    dest: "{{ item.dest_path }}"
    mode: "{{ item.mode if item.mode is defined else 'preserve' }}"
  loop: "{{ monitor_custom_files_paths }}"
  tags: reconfigure

- name: Reconfigure GitLab
  command: gitlab-ctl reconfigure
  tags: reconfigure

- name: Propagate Secrets if new
  include_role:
    name: common
    tasks_from: secrets
  vars:
    gitlab_secrets_reconfigure: true
  tags:
    - reconfigure
    - secrets

- name: Restart Monitor
  shell: |
    gitlab-ctl stop
    pkill -f "/opt/gitlab/embedded/bin/prometheus[[:alnum:][:space:]\-]+" || echo "Prometheus is already stopped"
    gitlab-ctl start
  register: result
  retries: 2
  until: result is success
  tags:
    - reconfigure
    - restart
    - dashboards

- name: Create skip-auto-reconfigure file
  file:
    path: /etc/gitlab/skip-auto-reconfigure
    state: touch
    mode: u=rw,g=r,o=r

- name: Run Custom Tasks
  block:
    - name: Check if Custom Tasks file exists
      stat:
        path: "{{ monitor_custom_tasks_file }}"
      register: monitor_custom_tasks_file_path
      delegate_to: localhost
      become: false

    - name: Run Custom Tasks
      include_tasks:
        file: "{{ monitor_custom_tasks_file }}"
        apply:
          tags: custom_tasks
      when: monitor_custom_tasks_file_path.stat.exists
  tags: custom_tasks
